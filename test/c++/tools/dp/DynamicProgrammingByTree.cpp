// Copyright (C) 2019 EDF
// All Rights Reserved
// This code is published under the GNU Lesser General Public License (GNU LGPL)
#include <fstream>
#include <memory>
#include <functional>
#include <array>
#include <boost/lexical_cast.hpp>
#include <Eigen/Dense>
#include "geners/BinaryFileArchive.hh"
#include "StOpt/tree/Tree.h"
#include "StOpt/core/grids/FullGrid.h"
#include "StOpt/dp/FinalStepDP.h"
#include "StOpt/dp/TransitionStepTreeDP.h"
#include "StOpt/dp/OptimizerDPTreeBase.h"
#include "StOpt/dp/SimulatorDPBaseTree.h"

using namespace std;


double  DynamicProgrammingByTree(const shared_ptr<StOpt::FullGrid> &p_grid,
                                 const shared_ptr<StOpt::OptimizerDPTreeBase > &p_optimize,
                                 const function<double(const int &, const Eigen::ArrayXd &, const Eigen::ArrayXd &)>  &p_funcFinalValue,
                                 const Eigen::ArrayXd &p_pointStock,
                                 const int &p_initialRegime,
                                 const string   &p_fileToDump
                                )
{
    // from the optimizer get back the simulator
    shared_ptr< StOpt::SimulatorDPBaseTree> simulator = p_optimize->getSimulator();
    // final values
    vector< shared_ptr< Eigen::ArrayXXd > >  valuesNext = StOpt::FinalStepDP(p_grid, p_optimize->getNbRegime())(p_funcFinalValue, simulator->getNodes());
    shared_ptr<gs::BinaryFileArchive> ar = make_shared<gs::BinaryFileArchive>(p_fileToDump.c_str(), "w");
    // name for object in archive
    string nameAr = "Continuation";
    // iterate on time steps
    for (int iStep = 0; iStep < simulator->getNbStep(); ++iStep)
    {
        simulator->stepBackward();
        // probabilities
        std::vector<double>  proba = simulator->getProba();
        // get connection between nodes
        std::vector< std::vector<std::array<int, 2>  > >  connected = simulator->getConnected();
        // conditional expectation operator
        shared_ptr<StOpt::Tree> tree = std::make_shared<StOpt::Tree>(proba, connected);
        // transition object
        StOpt::TransitionStepTreeDP transStep(p_grid, p_grid, p_optimize);
        pair< vector< shared_ptr< Eigen::ArrayXXd > >, vector< shared_ptr< Eigen::ArrayXXd > > > valuesAndControl = transStep.oneStep(valuesNext, tree);
        // dump continuation values
        transStep.dumpContinuationValues(ar, nameAr, iStep, valuesNext, valuesAndControl.second, tree);
        valuesNext = valuesAndControl.first;

    }
    // interpolate at the initial stock point and initial regime
    return (p_grid->createInterpolator(p_pointStock)->applyVec(*valuesNext[p_initialRegime])).mean();
}
