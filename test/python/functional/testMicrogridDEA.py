# Copyright (C) 2018 The Regents of the University of California, Michael Ludkovski and Aditya Maheshwari
# All Rights Reserved
# This code is published under the GNU Lesser General Public License (GNU LGPL)

from __future__ import division
import numpy as np
import math
import matplotlib.pyplot as plt
import time
import sys
from datetime import datetime
if (sys.version_info > (3, 0)):
        import pickle
else:
        import cPickle as pickle
       
import microgridDEA.parameters as bp
import microgridDEA.calEngineBatched as ce
import microgridDEA.forwardSimulations as fs



def runSimulation():

	# create an instance of parameter class.
	param = bp.basicVariables()

	# creates the list of objects defining the type of regression monte carlo.
	regParamObjcs = []
	for i in range(param.nstep):
		# for time steps from 0 to "param.designChangePnt-1", we use regress now-2D
		if i<param.designChangePnt:		
			regType = bp.regressionType('regress now 2D')
			regParamObjcs.append(regType)	
		else:
			# for time steps after "param.designChangePnt", we use grid-discretization
			regType = bp.regressionType('gd')
			regParamObjcs.append(regType)	

	# dump the regression parameters using pickle.
	name = param.regParamDump
	with open(name, 'wb') as output:
		pickle.dump(regParamObjcs, output, pickle.HIGHEST_PROTOCOL)

	ce.storageCalculation(param,regParamObjcs)

if __name__ == "__main__":

	runSimulation()	
	fs.forwardSimulations()
