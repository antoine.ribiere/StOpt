# Copyright (C) 2018 The Regents of the University of California, Michael Ludkovski and Aditya Maheshwari
# All Rights Reserved
# This code is published under the GNU Lesser General Public License (GNU LGPL)

from __future__ import division
import numpy as np
        
class basicVariables:


    def __init__(self, maturity=10):

        self.K = 10 #switching cost

        self.I0=5 #initial inventory

        self.I_minMax = [0,10] #Level of charge of the battery min max in KW
        self.B_minMax =  [-6,6] #battery min max output, positive implies output and negative implies input

        self.H = 1 # represents the number of regimes which in this case is 2, as a result H=1

        # assuming the cost function is of the type 
        # c1*d^a + c2*1{st>0} + c3*st*1{st<0}
        c =[1,10000,5] # assuming the cost function is of the type , c[0]*d^a + c[1]*1{st>0} + c[2]*1{st<0}(-st)
        self.a = 0.9
        self.c1 = c[0]
        self.c2 = c[1]
        self.c3 = c[2]


        self.maturity = maturity #unit in hours
        self.dt = 15/60 #unit in hours
        self.nstep = int(self.maturity/self.dt) #number of macro steps


        # demand parameters
        self.lambd = 0.5 #mean reversion rate
        self.sigma = 2 #volatility
        self.ampl = 6 
        self.mu = 0

        # we bound the maximum demand. This parameter defines the maximum possible demand.
        self.DemandUB = 10

        # file to dump the continuation values.
        self.filetoDump = "condExp"
        # file to dump the regression parameters.        
        self.regParamDump = 'regressionParameters.pkl'

        # parameter defines when to change the type of regression. 
        self.designChangePnt = 30


# regression related parameters
class regressionType:


    def __init__(self,rmctype='regress now 2D'):

        #number of meshes on demand dimension
        self.meshX = 4 
        #number of meshes on inventory dimension
        self.meshI = 10 
        # type of regression monte carlo i.e grid discretization or 2D regressions.
        self.rmctype = rmctype 
        
        if self.rmctype == 'gd':
            self.regType = 'piecewiseLinear' #regression type.
            # number of simulations
            self.nbsimulOpt = 500*self.meshX 

            #number of unique design sites.
            self.nbUniqueSite = 500*self.meshX 
            #batch size.     
            self.batchSize = 1  

            # exit the code if the product of the batch size and unique sites is not equal to number of simulations.
            if (self.nbUniqueSite*self.batchSize != self.nbsimulOpt):
                print("wrong budget specification: batch size and sites don't match the total simulations")
                exit(0)
            

        elif self.rmctype == 'regress now 2D':
            self.regType =  'globalPolynomial'  #'piecewiseLinear' ##'kernel' 
            self.nbsimulOpt = 10000

            #number of unique design sites.
            self.nbUniqueSite = 1000
            #batch size.
            self.batchSize = 10

            # exit the code if the product of the batch size and unique sites is not equal to number of simulations.
            if (self.nbUniqueSite*self.batchSize != self.nbsimulOpt):
                print("wrong budget specification: batch size and sites don't match the total simulations")
                exit(0)

            self.degree= 2  # used only with regType = 'globalPolynomial'

            # used only with regType = 'kernel'
            self.bandwidth = 0.2
            # factor for the number of points
            self.factPoint=1.

        
